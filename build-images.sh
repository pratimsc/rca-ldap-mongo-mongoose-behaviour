#!/usr/bin/env bash

docker build -f infra/api-app-v4.dockerfile -t mmcrad/rca-api-app-01:4 .
docker build -f infra/api-app-v5.dockerfile -t mmcrad/rca-api-app-01:5 .

docker push mmcrad/rca-api-app-01:4
docker push mmcrad/rca-api-app-01:5
