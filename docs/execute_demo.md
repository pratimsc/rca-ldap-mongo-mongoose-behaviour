# Running the Demo Applications

## Prerequisite
To enable co-existence of multiple environment, and easy creation/tearing down of ephemeral environments, a [*Kubernetes*](https://kubernetes.io/) based hosting environment is used.

One can also use AWS environment to create a EKS based Kubernetes cluster. The repository [*Kubernetes Cluster creation on AWS*](https://bitbucket.org/pratimsc/poc-cloud-kubernetes-aws/src) has scripts and instruction to create a personal cluster.

Alternatively, one may use [*minikube*](https://kubernetes.io/docs/tasks/tools/install-minikube/) to create a localized Kubernetes environment.
> Note: Administrative access are required to use minikube, on local machine.

The rest of the instruction in this doc is written with assumption that a AWS based kubernetes cluster is used.

## MongoDB Cluster
All the applications will required a MongoDB instance. In the available Kubernetes environment execute the below command to get an Mongodb database:
```shell script
kubectl apply infra/mongodb/mongodb.yaml
```
It will expose a MongoDB database, within the cluster at :
- Primary - `mongodb-nd-0.mongodb-cluster.rca-mongodb.svc.cluster.local:27017`
- Secondary - `mongodb-nd-1.mongodb-cluster.rca-mongodb.svc.cluster.local:27017`

The default user name is `admin` and password is `password`.

## Demo Apps Hosting

### Ring fencing and Namespace
All the applications will run in their own ring-fenced environment using Kubernetes inbuilt isolation mechanism i.e. Namespace.
Execute the below script:
```shell script
kubectl apply -f infra/apps/01-namespace.yaml
```

 ### App Core: Application with Mongo DB official library
- Application : [source](../api-app/index-core.js)
- Execute: `npm run core`
- Docker Image: ` mmcrad/rca-api-app-01:5`
- Containerized/Kubernetes: `kubectl apply -f infra/apps/02-api-app-core.yaml`

This application uses the Mongo DB official driver `3.4.1`, and offers *CRUD* APIs at end point `/api/employees`.

To know the service end point you can execute:
```shell script
kubectl -n rca-api-app01 get service api-app-core
```
You should get an output similar to below:
```shell script
NAME           TYPE           CLUSTER-IP      EXTERNAL-IP                                                              PORT(S)        AGE
api-app-core   LoadBalancer   10.100.223.94   a0b76a5a93b8c11ea8f0702bc5c95d60-496314092.us-east-1.elb.amazonaws.com   80:30345/TCP   47h
```
Then the service should be available at `http://a0b76a5a93b8c11ea8f0702bc5c95d60-496314092.us-east-1.elb.amazonaws.com/api/employees`.


### App Mongoose Default: Application with Mongoose library, with Default values
- Application : [source](../api-app/index-mongoose-default.js)
- Execute: `npm run m5-default`
- Docker Image: ` mmcrad/rca-api-app-01:5`
- Containerized/Kubernetes: `kubectl apply -f infra/apps/03-api-app-m5-default.yaml`

This application uses the Mongoose library `5.8.6`, and offers same *CRUD* APIs as above.

To know the service end point you can execute:
```shell script
kubectl -n rca-api-app01 get service api-app-m5-default
```
The command will provide the external ip. The service should be available at `http://<<external ip>>/api/employees`.


### App Mongoose Conf: Application with Mongoose library, with explicit resiliency
- Application : [source](../api-app/index-mongoose-resilient.js)
- Execute: `npm run m5-resilient`
- Docker Image: ` mmcrad/rca-api-app-01:5`
- Containerized/Kubernetes: `kubectl apply -f infra/apps/04-api-app-m5-resilient.yaml`

This application uses the Mongoose library `5.8.6`, and offers same *CRUD* APIs as above.

To know the service end point you can execute:
```shell script
kubectl -n rca-api-app01 get service api-app-m5-resilient
```
The command will provide the external ip. The service should be available at `http://<<external ip>>/api/employees`.


### App Mongoose V4: Application with Mongoose **v4** library
- Application : [source](api-app/index-mongoose-v4.js)
- Execute: `npm run m4` 
- Docker Image: ` mmcrad/rca-api-app-01:4`
- Containerized/Kubernetes: `kubectl apply -f infra/apps/05-api-app-m4.yaml`

This application uses the Mongoose library `4.7.4`, and offers same *CRUD* APIs as above.

To know the service end point you can execute:
```shell script
kubectl -n rca-api-app01 get service api-app-m4
```
The command will provide the external ip. The service should be available at `http://<<external ip>>/api/employees`.

## Tearing Down Environment

### Tearing down the application
You can execute the following command to tear down each of the applications, individually:
```shell script
kubectl delete -f infra/apps/05-api-app-m4.yaml
kubectl delete -f infra/apps/04-api-app-m5-resilient.yaml
kubectl delete -f infra/apps/03-api-app-m5-default.yaml
kubectl delete -f infra/apps/02-api-app-core.yaml
kubectl delete -f infra/apps/01-namespace
```
Or just execute `kubectl delete -f infra/apps/`

### Tearing down the Mongodb
Execute the bellow command to delete the MongoDb environment:
```shell script
kubectl delete -f infra/mongodb
``` 
