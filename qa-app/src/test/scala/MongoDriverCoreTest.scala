
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.concurrent.duration._

class MongoDriverCoreTest extends Simulation{
  private val baseUrl = "http://a0b76a5a93b8c11ea8f0702bc5c95d60-496314092.us-east-1.elb.amazonaws.com"
  private val contentType = "application/json"
  private val endpoint = "/api/employees"
  private val requestCount = 100
  private val requestDuration = 10 minutes

  val httpProtocol = http
    .baseUrl(baseUrl)
    .inferHtmlResources()
    .acceptHeader("*/*")
    .contentTypeHeader(contentType)
    .userAgentHeader("curl/7.54.0")

  val scn = scenario("MongoDriverCoreTest")
    .exec(http("request_0")
      .get(endpoint)
      .check(status.is(200)))

  setUp(scn.inject(rampUsersPerSec(1) to(requestCount) during(requestDuration))).protocols(httpProtocol)

}
