
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class MongooseV4Test extends Simulation{
  private val baseUrl = "http://a3751be653d1511ea8f0702bc5c95d60-1247329345.us-east-1.elb.amazonaws.com"
  private val contentType = "application/json"
  private val endpoint = "/api/employees"
  private val requestCount = 100
  private val requestDuration = 10 minutes

  val httpProtocol = http
    .baseUrl(baseUrl)
    .inferHtmlResources()
    .acceptHeader("*/*")
    .contentTypeHeader(contentType)
    .userAgentHeader("curl/7.54.0")

  val scn = scenario("MongooseConfTest")
    .exec(http("request_0")
      .get(endpoint)
      .check(status.is(200)))

  setUp(scn.inject(rampUsersPerSec(1) to(requestCount) during(requestDuration))).protocols(httpProtocol)

}
