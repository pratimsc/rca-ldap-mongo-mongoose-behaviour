
import scala.concurrent.duration._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

class MongooseDefaultTest extends Simulation{
  private val baseUrl = "http://a0c2510e93b8c11ea8f0702bc5c95d60-1214775870.us-east-1.elb.amazonaws.com"
  private val contentType = "application/json"
  private val endpoint = "/api/employees"
  private val requestCount = 100
  private val requestDuration = 10 minutes

  val httpProtocol = http
    .baseUrl(baseUrl)
    .inferHtmlResources()
    .acceptHeader("*/*")
    .contentTypeHeader(contentType)
    .userAgentHeader("curl/7.54.0")

  val scn = scenario("MongooseDefaultTest")
    .exec(http("request_0")
      .get(endpoint)
      .check(status.is(200)))

  setUp(scn.inject(rampUsersPerSec(1) to(requestCount) during(requestDuration))).protocols(httpProtocol)

}
