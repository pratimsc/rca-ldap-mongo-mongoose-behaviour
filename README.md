# RCA for MongoDB and Mongoose interaction issue 
> Note: This code in the repository is for experimentation and investigation, and should not used as-it-is in any production environment. 

## Purpose
It was been observed in a product deployed application that, the way it uses Mongoose driver, it could not always handle MongoDB server outages gracefully. The application would not recover and serve requests, when the MongoDB database becomes reachable again, after some kind of outages. The reason of outage did not have any affect on this.

This repository contains sample codes to simulate the issue, seen in production, in a localized environment.  
The repository will have 4 sample applications, each exposing following APIs:
- `GET /api/employees` : returns a list of employees in the database
- `POST /api/employees` : inserts an employee in the database
- `PUT /api/employees/:id` : updates an employee in the database, when their id matches with one provided by `:id`
- `DELETE /api/employees/:id` : deletes an employee in the database, when their id matches with one provided by `:id`

The sample applications are :
- App Core: 
  This is an application that uses office driver from MongoDB.
- App Mongoose Default:
  This is an application that uses Mongoose v5 driver, and just uses the default values for database connection.
- App Mongoose Conf:
  This is an application that uses Mongoose v5 driver, and explicitly mentions database connection related values. 
- App Mongoose v4:
  This is an application that uses Mongoose v4 driver, and simulates the usage as done by the problematic production application.

## Execution
The document named [`execute_demo.md`](docs/execute_demo.md) has required instruction to execute the demo apps.

## Application Probing
The document name [`execute_test.md`](docs/execute_test.md) has required instruction to execute tests and re-produce the problem.

# Conclusion
## Outcome
In the development environment, the problem could not be reproduced. The application built with version *4.7.4* and *5.8.6* responds to Mongodb outage in a reactive way. They will continue to attempt re-connection until a successful connection with Mongodb is established. Once, an connection is made the apps are responding positively with data from Mongo database.

## Observation
 In 2016, issue [#4581](https://github.com/Automattic/mongoose/issues/4581) was reported to Mongoose team. This reported issue is very similar what has been reported in the production environment. This issue fixed as part of release `4.6.5`. 
 So this issue should not occur in higher version. However, in case its noticed, then below steps need to be taken :
 - Disable mongoose buffering with `bufferCommands: false` in the schema
 - Set the driver's `bufferMaxEntries` to 0 like `mongoose.connect(uri, { db: { bufferMaxEntries: 0 } })`

# References:
- https://kubernetes.io/docs/tasks/tools/install-minikube/
