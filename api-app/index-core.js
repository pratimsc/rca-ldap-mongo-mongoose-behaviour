const express = require('express');
const app = express();
const port = process.env.APP_PORT || 8300;

// Mongodb related stuffs
const MongoClient = require('mongodb').MongoClient;
const ObjectID = require('mongodb').ObjectID;
const dbname = process.env.MONGO_DATABASE.trim() || "rca-app-01";
const username = process.env.MONGO_USERNAME.trim() || "admin";
const password = process.env.MONGO_PASSWORD.trim() || "password";
const mongoServer = process.env.MONGO_SERVER.trim() || "127.0.0.1";
const mongoPort = process.env.MONGO_PORT || 27017;
const postUri = process.env.MONGO_URI_POSTFIX || "";
const uri = "mongodb://" + username + ":" + password + "@" + mongoServer + ":" + mongoPort + "/" + dbname + postUri;


console.info("Mongo DB URI:" + uri)

// Use json for data exchange
app.use(express.json());

app.get('/', (req, res) => {
    res.status(200).send('I am alive!')
});

app.get('/api/employees', (req, res) => {
    const client = new MongoClient(uri, {useNewUrlParser: true});
    client.connect(err => {
        if (err) res.status(503).send(err);
        const collection = client.db(dbname).collection("employees");
        collection.find().toArray((error, documents) => {
            if (error) res.status(503).send(error);
            res.status(200).send(documents);
        });
        client.close();
    });
});

app.get('/api/employees/:employeeId', (req, res) => {
    console.info('Fetching information for employee ' + req.params.employeeId);
    const client = new MongoClient(uri, {useNewUrlParser: true});
    client.connect(err => {
        if (err) res.status(503).send(err);
        const collection = client.db(dbname).collection("employees");
        collection.find({_id: ObjectID(req.params.employeeId)}).toArray((error, documents) => {
            if (error) res.status(503).send(error);
            res.status(200).send(documents);
        });
        client.close();
    });
});

app.post('/api/employees', (req, res) => {
    console.info('Adding an employee ' + req.body);
    const client = new MongoClient(uri, {useNewUrlParser: true});
    client.connect(err => {
        if (err) res.status(503).send(err);
        const collection = client.db(dbname).collection("employees");
        collection.insertOne(req.body, (error, result) => {
            if (error) res.status(503).send(error);
            res.status(201).send(result);
        });
        client.close();
    });
});

app.put('/api/employees/:employeeId', (req, res) => {
    console.info('Updating an employee ' + req.body);
    const client = new MongoClient(uri, {useNewUrlParser: true});
    client.connect(err => {
        if (err) res.status(503).send(err);
        const collection = client.db(dbname).collection("employees");
        collection.replaceOne({_id: ObjectID(req.params.employeeId)}, req.body, (error, result) => {
            if (error) res.status(503).send(error);
            res.status(200).send(result);
        });
        client.close();
    });
});

app.delete('/api/employees/:employeeId', (req, res) => {
    console.info('Deleting an employee ' + req.body);
    const client = new MongoClient(uri, {useNewUrlParser: true});
    client.connect(err => {
        if (err) res.status(503).send(err);
        const collection = client.db(dbname).collection("employees");
        collection.deleteOne({_id: ObjectID(req.params.employeeId)}, (error, result) => {
            if (error) res.status(503).send(error);
            res.status(204).send(result);
        });
        client.close();
    });
});

app.listen(port, () => console.log("listening on " + port));
