const express = require('express');
const app = express();
const port = process.env.APP_PORT || 8300;
const dbname = process.env.MONGO_DATABASE.trim() || "rca-app-01";
const username = process.env.MONGO_USERNAME.trim() || "admin";
const password = process.env.MONGO_PASSWORD.trim() || "password";
const mongoServer = process.env.MONGO_SERVER.trim() || "127.0.0.1";
const mongoPort = process.env.MONGO_PORT || 27017;
const postUri = process.env.MONGO_URI_POSTFIX || "";
const uri = "mongodb://" + username + ":" + password + "@" + mongoServer + ":" + mongoPort + "/" + dbname + postUri;

console.info("Mongo DB URI:" + uri);

// Mongodb related stuffs
const ObjectID = require('mongodb').ObjectID;

// Mongoose related stuffs
const mongoose = require('mongoose');
mongoose.Promise = global.Promise;
mongoose.connect(uri, {useNewUrlParser: true, useCreateIndex: true});

mongoose.connection.on('error', err => {
    console.error('MongoDB connection error: ' + err);
    process.exit(-1);
});
mongoose.connection.on('reconnectFailed', () => {
    console.error('MongoDB reconnection failed.');
});
mongoose.connection.on('timeout', () => {
    console.warn('timeout event emitted!')
});

mongoose.connection.on('open', () => {
    console.info("Connection open ..........")
});
mongoose.connection.on('connected', () => {
    console.info('MongoDB connection open to ' + uri);
});
mongoose.connection.on('disconnected', () => {
    console.warn('MongoDB connection disconnected');
});

process.on('SIGINT', () => {
    mongoose.connection.close(() => {
        console.log('MongoDB connection disconnected through app termination');
        process.exit(0);
    });
});

// Documents
const employee = new mongoose.Schema({
    name: {
        type: String,
        required: true
    },
    age: {
        type: Number,
        required: true
    },
    description: {
        type: String,
        required: false
    }
});
const Employee = mongoose.model('Employees', employee);

// Use json for data exchange
// Ref: https://github.com/expressjs/express/wiki/Migrating-from-3.x-to-4.x
const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.status(200).send('I am alive!')
});

app.get('/api/employees', (req, res) => {
    Employee.find((error, employees) => {
        if (error) res.status(503).send(error);
        console.log(employees);
        res.status(200).send(employees);
    });
});

app.get('/api/employees/:employeeId', (req, res) => {
    console.info('Fetching information for employee ' + req.params.employeeId);
    Employee.findOne({_id: ObjectID(req.params.employeeId)}, (error, employee) => {
        if (error) res.status(503).send(error);
        res.status(200).send(employee);
    });
});

app.post('/api/employees', (req, res) => {
    console.info('Adding an employee ' + req.body);
    Employee.collection.insertOne(req.body, (error, result) => {
        if (error) res.status(400).send(error);
        res.status(201).send(result);
    });
});

app.put('/api/employees/:employeeId', (req, res) => {
    console.info('Adding an employee ' + req.body);
    Employee.collection.replaceOne({_id: ObjectID(req.params.employeeId)}, req.body, (error, result) => {
        if (error) res.status(503).send(error);
        res.status(200).send(result);
    });
});

app.delete('/api/employees/:employeeId', (req, res) => {
    console.info('Deleting an employee ' + req.body);
    Employee.collection.deleteOne({_id: ObjectID(req.params.employeeId)}, (error, result) => {
        if (error) res.status(503).send(error);
        res.status(204).send(result);
    });
});

app.listen(port, () => console.log("listening on " + port));
