FROM node:12

MAINTAINER Pratim SC <pratim.sunilkumar.chaudhuri@mercer.com>

ENV MSRV_HOME /microservice

USER root
COPY infra/id_rsa /home/node/.ssh/id_rsa
COPY api-app/* ${MSRV_HOME}/src/
RUN chown -R node:node /home/node/.ssh \
    && chmod 700 /home/node/.ssh \
    && chmod 600 /home/node/.ssh/id_rsa \
    && chown -R node:node ${MSRV_HOME}/src

USER node
WORKDIR ${MSRV_HOME}/src
RUN mv package-m5.json package.json \
    && npm install --production \
    && node --version \
    && npm --version

CMD [ "npm", "run", "core" ]
